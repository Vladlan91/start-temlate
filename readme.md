
![Иллюстрация к проекту](https://github.com/Vladlan91/start-temlate/raw/app/img/preview.png)
# START TEMPALTE
Создание качественной темы требует сложной работы и длительного времени. В то время как базовая структура и некоторые файлы являются общими для всех тем, они (темы) варьируются по своим уникальным возможностям и особенностям. Выполнять однотипные предварительные задачи всякий раз при создании темы – это то же самое, что всякий раз заново изобретать колесо перед созданием автомобиля. Стартовые темы обеспечивают базовые файлы и структуру для придания импульса развитию вашего проекта.
[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)
